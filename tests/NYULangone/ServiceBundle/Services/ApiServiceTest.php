<?php

namespace App\Tests\NYULangone\ServiceBundle\Services;

use App\NYULangone\ServiceBundle\Services\ApiService;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

class ApiServiceTest extends TestCase
{
    public function testAdd()
    {
        $api = new ApiService(new Client, 'https://yandex.ru/robots.txt');
        $result = '200';

        // assert that your calculator added the numbers correctly!
        $this->assertEquals($api->getResponseYandexRobots()->getStatusCode(), $result);
    }
}