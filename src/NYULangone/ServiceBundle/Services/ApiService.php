<?php

namespace App\NYULangone\ServiceBundle\Services;


use GuzzleHttp\Client;

class ApiService {

  protected $client;
  protected $urlYandex;

  /**
   * ApiService constructor.
   */
  public function __construct(Client $client, $urlYandex) {
    $this->client = $client;
    $this->urlYandex = $urlYandex;
  }

  public function getYandexRobots() {
    $response = $this->client->get($this->urlYandex);

    return $response->getBody();
  }

  public function getResponseYandexRobots() {
    $response = $this->client->get($this->urlYandex);

    return $response;
  }

  public function getResume() {
    $response = $this->client->request('GET', 'http://cms.it-resume.local:8080/api_v2/resume/52');

    return $response;
  }
}