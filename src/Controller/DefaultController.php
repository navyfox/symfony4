<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\NYULangone\ServiceBundle\Services\ApiService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
// use Symfony\Component\HttpFoundation\Response;
// use Psr\Http\Message\ServerRequestInterface;
// use Zend\Diactoros\Response;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;

class DefaultController extends Controller {

  protected $api;

  
  public function __construct(ApiService $api) {
    $this->api = $api;
  }

  /**
  * @Route("/{reactRouting}", name="index", requirements={"reactRouting"="^(?!api).+"}, defaults={"reactRouting": null})
  */
  public function index() {
    return $this->render('index.html.twig', [
      'body' => $this->api->getYandexRobots()
    ]);
  }

  /**
  * @Route("/api/action1", name="proxy1")
  */
  public function apiAction()
  {
    $httpFoundationFactory = new HttpFoundationFactory();
    $response = $this->api->getResume();
    $symfonyResponse = $httpFoundationFactory->createResponse($response);
    return $symfonyResponse;
  }

  /**
  * @Route("/api/action2", name="proxy2")
  */
  public function apiAction2()
  {
    $json = $this->api->getResume()->getBody();
    $response = new JsonResponse();
    $response = JsonResponse::fromJsonString($json);
    return $response;
  }
}